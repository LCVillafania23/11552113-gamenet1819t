﻿using UnityEngine;
using UnityEngine.Networking;

public class PlayerController : NetworkBehaviour {
    public GameObject bulletPrefab;
    public Transform bulletSpawn;

    [SerializeField]
    private float ForwardSpeed, RotationSpeed;

    void Update() {
        if (!isLocalPlayer) {
            return;
        }
        float forwardForce = Input.GetAxis("MoveForward") * Time.deltaTime * RotationSpeed;
        float rotationForce = Input.GetAxis("Rotate") * Time.deltaTime * ForwardSpeed;

        transform.Translate(0, 0, forwardForce);
        transform.Rotate(0, rotationForce, 0);

        if (Input.GetKeyDown(KeyCode.Space)) {
            CmdFire();
        }
    }

    public override void OnStartLocalPlayer() {
        GetComponent<MeshRenderer>().material.color = Color.blue;
    }

    [Command]
    void CmdFire() {
        float bulletSpeed = 6f;
        float delay = 2f;
        
        GameObject bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
        
        bullet.GetComponent<Rigidbody>().velocity = bullet.transform.forward * bulletSpeed;

        NetworkServer.Spawn(bullet);

        Destroy(bullet, delay);
    }
}