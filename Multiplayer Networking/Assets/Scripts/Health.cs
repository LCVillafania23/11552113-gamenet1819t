﻿using UnityEngine;
using UnityEngine.Networking;

public class Health : NetworkBehaviour {
    public RectTransform HealthBar;
    public const int MaxHealth = 100;

    [SyncVar(hook = "OnChangeHealth")]
    public int CurrentHealth = MaxHealth;
    public bool destroyOnDeath;

    private NetworkStartPosition[] spawnPoints;

    void Start() {
        if (isLocalPlayer) {
            spawnPoints = FindObjectsOfType<NetworkStartPosition>();
        }
    }

    public void TakeDamage(int amount) {
        if (!isServer)
            return;

        CurrentHealth -= amount;

        if (CurrentHealth <= 0) {
            if (destroyOnDeath) {
                Destroy(gameObject);
            }
            else {
                CurrentHealth = MaxHealth;
                RpcRespawn();
            }
        }
    }

    void OnChangeHealth(int health) {
        HealthBar.sizeDelta = new Vector2(health, HealthBar.sizeDelta.y);
    }

    [ClientRpc]
    void RpcRespawn() {
        if (isLocalPlayer) {
            Vector3 spawnPoint = Vector3.zero;

            if (spawnPoints != null && spawnPoints.Length > 0) {
                spawnPoint = spawnPoints[Random.Range(0, spawnPoints.Length)].transform.position;
            }
            transform.position = spawnPoint;
        }
    }
}