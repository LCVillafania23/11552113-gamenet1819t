﻿using UnityEngine;
public class Bullet : MonoBehaviour {

    void OnCollisionEnter(Collision collision) {
        GameObject hit = collision.gameObject;
        Health health = hit.GetComponent<Health>();
        int damage = 10;

        if (health != null) {
            health.TakeDamage(damage);
        }

        Destroy(gameObject);
    }
}